DROP TABLE IF EXISTS `biz_order`;
CREATE TABLE `biz_order` (
  `id`          int(11) NOT NULL AUTO_INCREMENT
  COMMENT '主键',
  `goods_name`  varchar(255)     DEFAULT NULL
  COMMENT '商品名称',
  `place`       varchar(255)     DEFAULT NULL
  COMMENT '下单地点',
  `create_time` datetime         DEFAULT NULL
  COMMENT '下单时间',
  `user_name`   varchar(255)     DEFAULT NULL
  COMMENT '下单用户名称',
  `user_phone`  varchar(255)     DEFAULT NULL
  COMMENT '下单用户电话',
  PRIMARY KEY (`id`) USING BTREE
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8
  ROW_FORMAT = DYNAMIC
  COMMENT = '订单表';
SET FOREIGN_KEY_CHECKS = 1;